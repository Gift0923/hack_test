def caesarCipher(s, k):
    # Write your code here
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    k = k%26
    first = alphabet[k:]
    sec = alphabet[:k]
    rotatings = first + sec 
    dicshift = {}
    for i in range(len(alphabet)):
        dicshift[alphabet[i]] = rotatings[i]
    shift = ""
    for i in s:
        if i.isupper():
            letter = dicshift[i.lower()]
            shift += letter.upper()
        elif i.islower():
            shift += dicshift[i]
        else:
            shift += i 
    return shift
